#!/usr/bin/env bats

source archivo1.sh

@test "funcion1" {
 
  [ "$(funcion1)" -eq 1 ]
}

@test "funcion2" {
  [ "$(funcion2)" -eq 2 ]
}
